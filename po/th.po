# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Theppitak Karoonboonyanan <theppitak@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-03 21:00+0200\n"
"PO-Revision-Date: 2017-09-23 19:10+0000\n"
"Last-Translator: Theppitak Karoonboonyanan <theppitak@gmail.com>\n"
"Language-Team: Thai (http://www.transifex.com/xfce/xfce-panel-plugins/language/th/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: th\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../panel-plugin/fsguard.c:252
msgid "Unable to find an appropriate application to open the mount point"
msgstr "ไม่สามารถหาโปรแกรมที่เหมาะสมสำหรับเปิดจุดเมานท์นี้"

#: ../panel-plugin/fsguard.c:288 ../panel-plugin/fsguard.c:289
#, c-format
msgid "%.2f GB"
msgstr "%.2f GB"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s (%s)"
msgstr "เหลือเนื้อที่ %s/%s ใน %s (%s)"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s"
msgstr "เหลือเนื้อที่ %s/%s ใน %s"

#: ../panel-plugin/fsguard.c:295 ../panel-plugin/fsguard.c:296
#, c-format
msgid "%.0f MB"
msgstr "%.0f MB"

#: ../panel-plugin/fsguard.c:298
#, c-format
msgid "could not check mountpoint %s, please check your config"
msgstr "ไม่สามารถตรวจสอบจุดเมานท์ %s ได้ กรุณาตรวจสอบค่าตั้งของคุณ"

#: ../panel-plugin/fsguard.c:318
#, c-format
msgid "Only %s space left on %s (%s)!"
msgstr "เหลือเนื้อที่เพียง %s ใน %s (%s)!"

#: ../panel-plugin/fsguard.c:321
#, c-format
msgid "Only %s space left on %s!"
msgstr "เหลือเนื้อที่เพียง %s ใน %s!"

#. }}}
#. vim600: set foldmethod=marker: foldmarker={{{,}}}
#: ../panel-plugin/fsguard.c:635 ../panel-plugin/fsguard.desktop.in.h:1
msgid "Free Space Checker"
msgstr "มาตรวัดเนื้อที่ว่าง"

#: ../panel-plugin/fsguard.c:646
msgid "Configuration"
msgstr "ตั้งค่า"

#: ../panel-plugin/fsguard.c:653
msgid "Mount point"
msgstr "จุดเมานท์"

#: ../panel-plugin/fsguard.c:659
msgid "Warning limit (%)"
msgstr "ขีดเริ่มเตือน (%)"

#: ../panel-plugin/fsguard.c:664
msgid "Urgent limit (%)"
msgstr "ขีดเร่งด่วน (%)"

#: ../panel-plugin/fsguard.c:684
msgid "User Interface"
msgstr "ส่วนติดต่อผู้ใช้"

#: ../panel-plugin/fsguard.c:691
msgid "Name"
msgstr "ชื่อ"

#: ../panel-plugin/fsguard.c:699
msgid "Display size"
msgstr "แสดงขนาด"

#: ../panel-plugin/fsguard.c:703
msgid "Display meter"
msgstr "แสดงมาตร"

#: ../panel-plugin/fsguard.c:707
msgid "Display button"
msgstr "แสดงปุ่ม"

#: ../panel-plugin/fsguard.desktop.in.h:2
msgid "Monitor free disk space"
msgstr "เฝ้าสังเกตเนื้อที่ว่างในดิสก์"
