# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Kevin Brubeck Unhammer <unhammer+dill@mm.st>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-03 21:00+0200\n"
"PO-Revision-Date: 2017-09-19 18:06+0000\n"
"Last-Translator: Kevin Brubeck Unhammer <unhammer+dill@mm.st>\n"
"Language-Team: Norwegian Nynorsk (http://www.transifex.com/xfce/xfce-panel-plugins/language/nn/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nn\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/fsguard.c:252
msgid "Unable to find an appropriate application to open the mount point"
msgstr "Klarte ikkje finna eit passande program til å opna mappa til disken"

#: ../panel-plugin/fsguard.c:288 ../panel-plugin/fsguard.c:289
#, c-format
msgid "%.2f GB"
msgstr "%.2f GB"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s (%s)"
msgstr "%s/%s ledig på %s (%s)"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s"
msgstr "%s/%s ledig på %s"

#: ../panel-plugin/fsguard.c:295 ../panel-plugin/fsguard.c:296
#, c-format
msgid "%.0f MB"
msgstr "%.0f MB"

#: ../panel-plugin/fsguard.c:298
#, c-format
msgid "could not check mountpoint %s, please check your config"
msgstr "klarte ikkje sjekka monteringsstien %s, ver venleg og kontroller oppsettet"

#: ../panel-plugin/fsguard.c:318
#, c-format
msgid "Only %s space left on %s (%s)!"
msgstr "Berre %s ledig på %s (%s)!"

#: ../panel-plugin/fsguard.c:321
#, c-format
msgid "Only %s space left on %s!"
msgstr "Berre %s ledig på %s!"

#. }}}
#. vim600: set foldmethod=marker: foldmarker={{{,}}}
#: ../panel-plugin/fsguard.c:635 ../panel-plugin/fsguard.desktop.in.h:1
msgid "Free Space Checker"
msgstr "Sjekk ledig diskplass"

#: ../panel-plugin/fsguard.c:646
msgid "Configuration"
msgstr "Oppsett"

#: ../panel-plugin/fsguard.c:653
msgid "Mount point"
msgstr "Sti til disk"

#: ../panel-plugin/fsguard.c:659
msgid "Warning limit (%)"
msgstr "Varselgrense (%s)"

#: ../panel-plugin/fsguard.c:664
msgid "Urgent limit (%)"
msgstr "Faregrense (%s)"

#: ../panel-plugin/fsguard.c:684
msgid "User Interface"
msgstr "Brukargrensesnitt"

#: ../panel-plugin/fsguard.c:691
msgid "Name"
msgstr "Namn"

#: ../panel-plugin/fsguard.c:699
msgid "Display size"
msgstr "Vis storleik"

#: ../panel-plugin/fsguard.c:703
msgid "Display meter"
msgstr "Vis målar"

#: ../panel-plugin/fsguard.c:707
msgid "Display button"
msgstr "Vis knapp"

#: ../panel-plugin/fsguard.desktop.in.h:2
msgid "Monitor free disk space"
msgstr "Sjekk ledig diskplass"
