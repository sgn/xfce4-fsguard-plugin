# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Daniel Muñiz Fontoira <dani@damufo.com>, 2019
# Leandro Regueiro <leandro.regueiro@gmail.com>, 2006,2008-2010
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-03 21:00+0200\n"
"PO-Revision-Date: 2019-08-21 13:57+0000\n"
"Last-Translator: Daniel Muñiz Fontoira <dani@damufo.com>\n"
"Language-Team: Galician (http://www.transifex.com/xfce/xfce-panel-plugins/language/gl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: gl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/fsguard.c:252
msgid "Unable to find an appropriate application to open the mount point"
msgstr "Non se puido atopar unha aplicación axeitada para abrir o punto de montaxe"

#: ../panel-plugin/fsguard.c:288 ../panel-plugin/fsguard.c:289
#, c-format
msgid "%.2f GB"
msgstr "%.2f GB"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s (%s)"
msgstr "%s/%s de espazo libre en %s (%s)"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s"
msgstr "%s/%s de espazo libre en %s"

#: ../panel-plugin/fsguard.c:295 ../panel-plugin/fsguard.c:296
#, c-format
msgid "%.0f MB"
msgstr "%.0f MB"

#: ../panel-plugin/fsguard.c:298
#, c-format
msgid "could not check mountpoint %s, please check your config"
msgstr "non se puido comprobar o punto de montaxe %s, verifique a configuración"

#: ../panel-plugin/fsguard.c:318
#, c-format
msgid "Only %s space left on %s (%s)!"
msgstr "Só quedan %s libres en %s (%s)!"

#: ../panel-plugin/fsguard.c:321
#, c-format
msgid "Only %s space left on %s!"
msgstr "Só quedan %s libres en %s!"

#. }}}
#. vim600: set foldmethod=marker: foldmarker={{{,}}}
#: ../panel-plugin/fsguard.c:635 ../panel-plugin/fsguard.desktop.in.h:1
msgid "Free Space Checker"
msgstr "Comprobador de espazo libre"

#: ../panel-plugin/fsguard.c:646
msgid "Configuration"
msgstr "Configuración"

#: ../panel-plugin/fsguard.c:653
msgid "Mount point"
msgstr "Punto de montaxe"

#: ../panel-plugin/fsguard.c:659
msgid "Warning limit (%)"
msgstr "Límite de advertencia (%)"

#: ../panel-plugin/fsguard.c:664
msgid "Urgent limit (%)"
msgstr "Límite de urxencia (%)"

#: ../panel-plugin/fsguard.c:684
msgid "User Interface"
msgstr "Interface de usuario"

#: ../panel-plugin/fsguard.c:691
msgid "Name"
msgstr "Nome"

#: ../panel-plugin/fsguard.c:699
msgid "Display size"
msgstr "Amosar o tamaño"

#: ../panel-plugin/fsguard.c:703
msgid "Display meter"
msgstr "Amosar medida"

#: ../panel-plugin/fsguard.c:707
msgid "Display button"
msgstr "Amosar botón"

#: ../panel-plugin/fsguard.desktop.in.h:2
msgid "Monitor free disk space"
msgstr "Monitor de espazo de disco libre"
